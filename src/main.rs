use std::fs;
use anyhow::anyhow;

use orion::aead;

#[path = "../test/test.rs"]
mod test;

fn encrypt_file(
    in_file: &str,
    out_file: &str,
    key: &aead::SecretKey) -> Result<(), anyhow::Error> {
    let file_data = fs::read(in_file)?;

    let encrypted_file = aead::seal(&key, file_data.as_slice())
        .map_err(|err| anyhow!("Encrypting file: {}", err))?;

    fs::write(&out_file, encrypted_file)?;

    Ok(())
}

fn decrypt_file(
    in_file: &str,
    out_file: &str,
    key: &aead::SecretKey) -> Result<(), anyhow::Error> {
    let file_data = fs::read(in_file)?;

    let decrypted_file = aead::open(&key, file_data.as_slice())
        .map_err(|err| anyhow!("Encrypting file: {}", err))?;

    fs::write(&out_file, decrypted_file)?;

    Ok(())
}

fn main() {
    println!("Hello, world!");

    let key = aead::SecretKey::default();
    let _ = encrypt_file("src/main.rs", "enc.txt", &key);
    let _ = decrypt_file("enc.txt", "dec.txt", &key);
}

