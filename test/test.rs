#[cfg(test)]
mod test {
    use super::super::*;
    use std::fs;

    use orion::aead;

    #[test]
    fn test_encrypt_file() {
        let key = aead::SecretKey::default();

        let _ = encrypt_file("plaintext.txt", "enc.txt", &key);
        let _ = decrypt_file("enc.txt", "dec.txt", &key);

        let plaintext = fs::read("../src/main.rs").unwrap();
        let decrypted = fs::read("enc.txt").unwrap();

        assert_eq!(plaintext, decrypted);
    }
}
